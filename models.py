from datetime import datetime

from sqlalchemy import desc

from __init__ import db


class Talk(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  talk = db.Column(db.Text, nullable=False)
  date = db.Column(db.DateTime, default=datetime.utcnow)
  user_id = db.Column(db.Integer, nullable=False)
  # add username db.ForeignKey('user.id')

  @staticmethod
  def newest(num):
    return Talk.query.order_by(desc(Talk.date)).limit(num)
  
  def __repr__(self):
    return "{}".format(self.talk)
##    return "'{}'    '{}'".format(self.talk, self.date)

class Guest_talk(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  talk = db.Column(db.Text, nullable=False)
  date = db.Column(db.DateTime, default=datetime.utcnow)
  user_id = db.Column(db.Integer, nullable=False)

  @staticmethod
  def newest(num):
    return Guest_talk.query.order_by(desc(Guest_talk.date)).limit(num)
  
  def __repr__(self):
    return "{}".format(self.talk)


  
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True)
##    talks = db.relationship('Talk', backref='user', lazy='dynamic')

    def __repr__(self):
      return '<User: %r>' % self.name
