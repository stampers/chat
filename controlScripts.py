#! /usr/bin/env python

from __init__ import app, db
from flask_script import Manager, prompt_bool

from models import User

manager = Manager(app)

@manager.command
def initdb():
  db.create_all()
  db.session.add(User(name="Вика"))
  db.session.add(User(name="Daniel"))
  db.session.add(User(name="Guest"))
  db.session.commit()
  print ('A new database was initialized')
  
@manager.command
def deletedb():
  if prompt_bool(
      "Do you really want to delete this database?"
  ):
    db.drop_all()
    print ('Database Dropped')

if __name__ == '__main__':
  manager.run()
