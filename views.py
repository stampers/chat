from flask import redirect, render_template, url_for, flash, jsonify
from __init__ import app, db
from models import User, Talk, Guest_talk
from forms import MessageForm

import random


logo = ["Welcome", "Simple Chat", "Hi", "another test", "Have a nice day"]

# fake login
def current_user():
  return 2


@app.route("/", methods=['GET', 'POST'])
@app.route("/chat", methods=['GET', 'POST'])
def chat():
  form = MessageForm()
  if form.validate_on_submit():
    sendM = form.message.data
    msg = Guest_talk(talk=sendM, user_id=2)
    db.session.add(msg)
    db.session.commit()
    return redirect(url_for('chat'))
  return render_template('chat.html', form=form, m='g')


@app.route('/index')
def index():
  return render_template('index.html', logo=logo[random.randint(0,4)], title="title variable test", text="message variable test")


@app.route("/vika_chat", methods=['GET', 'POST'])
def vika_chat():
  ##dan = User.query.filter_by(name="Daniel").first()
  form = MessageForm()
  if form.validate_on_submit():
    sendM = form.message.data
    msg = Talk(talk=sendM, user_id=current_user()) #user=logged_in_user()) #, date=datetime.utcnow())
    db.session.add(msg)
    db.session.commit()
    #flash("Message sent '{}'".format(sendM))
    return redirect(url_for('vika_chat'))
  return render_template('chat.html', form=form, m='v') #user=logged_in_user()
# new_messages=Talk.newest(6)
# new_messages=db.session(Talk.talk).all(),


@app.route('/vtest')
def vtest():
  mes = Talk.newest(6)
  mes_list = []
  for i in mes:
    mes_list.append(i.talk)
  return jsonify(mes_list)

@app.route('/gtest')
def gtest():
  mes = Guest_talk.newest(6)
  mes_list = []
  for i in mes:
    mes_list.append(i.talk)
  return jsonify(mes_list)


@app.errorhandler(404)
def page_not_found(e):
  return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(e):
  return render_template('500.html'), 500


  ## ideas
  # add a random message taken from an array and set it as the title
