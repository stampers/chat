## review wtf form validation
from flask_wtf import FlaskForm
from wtforms.fields import StringField
#from flask.ext.etf.html5 import URLField
from wtforms.validators import DataRequired, Length

class MessageForm(FlaskForm):
    message = StringField('message', validators=[DataRequired()]) #, Length(max=64)])
